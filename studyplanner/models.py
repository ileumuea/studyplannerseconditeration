from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.urls import reverse

class Semester(models.Model):
    semester_term = models.CharField(max_length=10)

    def __str__(self):
        return "Semester:%s" %(self.semester_term)


class Module(models.Model):
    semester = models.ForeignKey(Semester,on_delete=models.CASCADE)
    # id = models.IntegerField(primary_key=True)
    module_name = models.CharField(max_length=250)
    module_code = models.CharField(max_length=20)
    module_duration = models.CharField(max_length=20)

    def __str__(self):
        return "ID:%d      %s" % (self.id,self.module_name)

    def getCourseworks(self):
        courseworks = []
        for cw in Coursework.objects.all():
            if cw.module_id == self.id:
                if cw.cw_type == "cw":
                    courseworks.append(cw)
        return courseworks

    def getExams(self):
        exams = []
        for e in Coursework.objects.all():
            if e.module_id == self.id:
                if e.cw_type == "exam":
                    exams.append(e)
        return exams

    def getTasks(self):
        tasks = []
        for t in Task.objects.all():
            moduleid = t.coursework.module_id
            if moduleid == self.id:
                tasks.append(t)
        return tasks


class Coursework(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE,default="")
    # task = models.ForeignKey(Task,on_delete=models.CASCADE)
    # id = models.IntegerField(primary_key=True)
    cw_name = models.CharField(max_length=500)
    cw_date_set = models.DateField()
    cw_deadline = models.DateField()
    cw_weight = models.PositiveIntegerField()
    cw_type = models.CharField(max_length=10,default="")

    def __str__(self):
        return "ID:%s      %s" % (self.id,self.cw_name)

    def get_absolute_url(self):
        return reverse("studyplanner:detail",kwargs={"id":self.module.id})


class Task(models.Model):
    ACTIVITY_CHOICES = (
        (1, ("Studying")),
        (2, ("Programming")),
        (3, ("Writing")),
        (4, ("Analyse")),
        (5, ("Read"))
    )
    CRITERION_CHOICES=(
        (1,("Time Studied")),
        (2,("Chapters Covered")),
        (3,("Assignment completed")),
    )

    coursework = models.ForeignKey(Coursework, on_delete=models.CASCADE, default="")
    task_name = models.CharField(max_length=500)
    date_due = models.DateField()
    notes = models.CharField(default=" ",max_length=1000)
    time_that_will_be_spent = models.PositiveIntegerField(default=1)

    task_activity = models.IntegerField(choices=ACTIVITY_CHOICES,default=0)
    criterion_type = models.IntegerField(choices=CRITERION_CHOICES,default=0)
    requirement_criterion = models.PositiveIntegerField(default=1)
    readData = True

    def __str__(self):
        return "ID:%d   Task:%s"%(self.id,self.task_name)

    def get_absolute_url(self):
        return reverse("studyplanner:detail",kwargs={"id":self.id})

    def editTask(self, t):
        t = Task(t)



