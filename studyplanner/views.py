from django.http import HttpResponse, Http404, HttpResponseRedirect

from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.views.generic import CreateView, FormView

from .models import Semester, Module, Coursework, Task
from django.views import generic
from .forms import TaskModelForm
from .models import Task
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required


# Create your views here.

# shows all the semesters
# def index(request):
#     all_modules = Module.objects.all()
#     context = { 'all_modules':all_modules}
#     return render(request,'studyplanner/index.html',context)
class IndexView(generic.ListView):
    template_name = 'studyplanner/index.html'
    context_object_name = 'all_modules'
    def get_queryset(self):
        all_modules = Module.objects.all()
        return all_modules


class TaskCreateView(CreateView):
    model = Task
    form_class = TaskModelForm

    template_name =  'studyplanner/task_new.html'

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(TaskCreateView, self).form_valid(form)



# shows all the courseworks of specified module
# in case the user is tring to access an invalid module with the module_id "/module_id/"
# it will throw an error 404 exeption
def module_details(request,module_id):
    if ('Back' in request.POST):
        print("BACK")
        return HttpResponseRedirect('/studyplanner/')
    if request.method == "POST":
        print("post")
    m = get_object_or_404(Module,pk = module_id)
    current_module = m.module_name
    current_code = m.module_code
    courseworks = m.getCourseworks()
    exams = m.getExams()
    tasks = m.getTasks()

    context = { 'courseworks':courseworks,
                'exams':exams,
                'current_module':current_module,
                'current_code':current_code,
                'm':m,
                'tasks':tasks,
                }
    return render(request,'studyplanner/detail.html',context)


def add_task(request,module_id,coursework_id):
    if ('Cancel' in request.POST):
        print("BACK")
        return HttpResponseRedirect('/studyplanner/' + str(module_id))

    if request.method == "POST":
        form = TaskModelForm(request.POST)
        if form.is_valid():
            t = Task()
            # saves a task object
            t.task_name = form.cleaned_data['task_name']
            t.date_due = form.cleaned_data['date_due']
            t.time_that_will_be_spent = form.cleaned_data['time_that_will_be_spent']
            t.task_activity = form.cleaned_data['task_activity']
            t.criterion_type = form.cleaned_data['requirement_criterion']
            t.notes = form.cleaned_data['notes']
            t.coursework = Coursework.objects.get(id = coursework_id)
            t.save()
            return HttpResponseRedirect('/studyplanner/'+str(module_id))
    else:
        form = TaskModelForm()
        module = Module.objects.get(pk=module_id)
        coursework = Coursework.objects.get(pk= coursework_id)
    return render(request,'studyplanner/task_new.html',{'form':form,'coursework':coursework,'module':module})

def edit_task(request, module_id, task_id):
    current_task = get_object_or_404(Task,pk=task_id)
    form = TaskModelForm(instance=current_task)

    if('Cancel' in request.POST):
        print("BACK")
        return HttpResponseRedirect('/studyplanner/' + str(module_id))

    if request.method =="GET":
        form = TaskModelForm(instance=current_task)
        return render(request, 'studyplanner/edit_task.html', {'edit_form': form})

    elif request.method == "POST":
        form = TaskModelForm(request.POST)
        if form.is_valid():
            print("change")
            t = Task.objects.get(pk = task_id)
            name = form.cleaned_data['task_name']
            date = form.cleaned_data['date_due']
            time = form.cleaned_data['time_that_will_be_spent']
            activity = form.cleaned_data['task_activity']
            criterion = form.cleaned_data['criterion_type']
            notes = form.cleaned_data['notes']
            if name != t.task_name:
                t.task_name = name
            if date != t.date_due:
                t.date_due=date
            if time != t.time_that_will_be_spent:
                t.time_that_will_be_spent==time
            if activity != t.task_activity:
                t.task_activity = activity
            if criterion != t.criterion_type:
                t.criterion_type = criterion
            if notes != t.notes:
                t.notes = notes
            t.save()
            return HttpResponseRedirect('/studyplanner/'+str(module_id))

