# Generated by Django 2.2.1 on 2019-05-11 21:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Coursework',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cw_name', models.CharField(max_length=500)),
                ('cw_date_set', models.DateField()),
                ('cw_deadline', models.DateField()),
                ('cw_weight', models.PositiveIntegerField()),
            ],
        ),
    ]
